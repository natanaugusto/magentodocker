version: '3'

networks:
  frontend:
    driver: ${NETWORKS_DRIVER}
  backend:
    driver: ${NETWORKS_DRIVER}

volumes:
  mysql:
    driver: ${VOLUMES_DRIVER}
  memcached:
    driver: ${VOLUMES_DRIVER}
  redis:
    driver: ${VOLUMES_DRIVER}
  mariadb:
    driver: ${VOLUMES_DRIVER}
  mongo:
    driver: ${VOLUMES_DRIVER}
  elasticsearch:
    driver: ${VOLUMES_DRIVER}

services:

### Workspace Utilities ##################################
    workspace:
      build:
        context: ./workspace
        args:
          - MAGENTODOCKER_PHP_VERSION=${PHP_VERSION}
          - MAGENTODOCKER_PHALCON_VERSION=${PHALCON_VERSION}
          - INSTALL_SUBVERSION=${WORKSPACE_INSTALL_SUBVERSION}
          - INSTALL_XDEBUG=${WORKSPACE_INSTALL_XDEBUG}
          - INSTALL_PHPDBG=${WORKSPACE_INSTALL_PHPDBG}
          - INSTALL_BLACKFIRE=${INSTALL_BLACKFIRE}
          - INSTALL_SSH2=${WORKSPACE_INSTALL_SSH2}
          - INSTALL_GMP=${WORKSPACE_INSTALL_GMP}
          - INSTALL_SOAP=${WORKSPACE_INSTALL_SOAP}
          - INSTALL_LDAP=${WORKSPACE_INSTALL_LDAP}
          - INSTALL_IMAP=${WORKSPACE_INSTALL_IMAP}
          - INSTALL_AMQP=${WORKSPACE_INSTALL_AMQP}
          - INSTALL_PHPREDIS=${WORKSPACE_INSTALL_PHPREDIS}
          - INSTALL_NODE=${WORKSPACE_INSTALL_NODE}
          - NPM_REGISTRY=${WORKSPACE_NPM_REGISTRY}
          - INSTALL_YARN=${WORKSPACE_INSTALL_YARN}
          - INSTALL_GRUNT=${WORKSPACE_INSTALL_GRUNT}
          - INSTALL_ZSH=${WORKSPACE_INSTALL_ZSH}
          - INSTALL_ZSH_MYZSH=${WORKSPACE_INSTALL_ZSH_MYZSH}
          - INSTALL_V8JS=${WORKSPACE_INSTALL_V8JS}
          - COMPOSER_GLOBAL_INSTALL=${WORKSPACE_COMPOSER_GLOBAL_INSTALL}
          - COMPOSER_REPO_PACKAGIST=${WORKSPACE_COMPOSER_REPO_PACKAGIST}
          - INSTALL_WORKSPACE_SSH=${WORKSPACE_INSTALL_WORKSPACE_SSH}
          - INSTALL_MC=${WORKSPACE_INSTALL_MC}
          - INSTALL_SYMFONY=${WORKSPACE_INSTALL_SYMFONY}
          - INSTALL_PYTHON=${WORKSPACE_INSTALL_PYTHON}
          - INSTALL_IMAGE_OPTIMIZERS=${WORKSPACE_INSTALL_IMAGE_OPTIMIZERS}
          - INSTALL_IMAGEMAGICK=${WORKSPACE_INSTALL_IMAGEMAGICK}
          - INSTALL_TERRAFORM=${WORKSPACE_INSTALL_TERRAFORM}
          - INSTALL_DUSK_DEPS=${WORKSPACE_INSTALL_DUSK_DEPS}
          - INSTALL_PG_CLIENT=${WORKSPACE_INSTALL_PG_CLIENT}
          - INSTALL_PHALCON=${WORKSPACE_INSTALL_PHALCON}
          - INSTALL_SWOOLE=${WORKSPACE_INSTALL_SWOOLE}
          - INSTALL_LIBPNG=${WORKSPACE_INSTALL_LIBPNG}
          - INSTALL_IONCUBE=${WORKSPACE_INSTALL_IONCUBE}
          - INSTALL_MYSQL_CLIENT=${WORKSPACE_INSTALL_MYSQL_CLIENT}
          - PUID=${WORKSPACE_PUID}
          - PGID=${WORKSPACE_PGID}
          - CHROME_DRIVER_VERSION=${WORKSPACE_CHROME_DRIVER_VERSION}
          - NODE_VERSION=${WORKSPACE_NODE_VERSION}
          - YARN_VERSION=${WORKSPACE_YARN_VERSION}
          - TZ=${WORKSPACE_TIMEZONE}
          - BLACKFIRE_CLIENT_ID=${BLACKFIRE_CLIENT_ID}
          - BLACKFIRE_CLIENT_TOKEN=${BLACKFIRE_CLIENT_TOKEN}
      volumes:
        - ${APP_CODE_PATH_HOST}:${APP_CODE_PATH_CONTAINER}
      extra_hosts:
        - "dockerhost:${DOCKER_HOST_IP}"
      ports:
        - "${WORKSPACE_SSH_PORT}:22"
      tty: true
      environment:
        - PHP_IDE_CONFIG=${PHP_IDE_CONFIG}
        - DOCKER_HOST=tcp://docker-in-docker:2375
      networks:
        - frontend
        - backend
      links:
        - docker-in-docker

### PHP-FPM ##############################################
    php-fpm:
      build:
        context: ./php-fpm
        args:
          - MAGENTODOCKER_PHP_VERSION=${PHP_VERSION}
          - MAGENTODOCKER_PHALCON_VERSION=${PHALCON_VERSION}
          - INSTALL_XDEBUG=${PHP_FPM_INSTALL_XDEBUG}
          - INSTALL_PHPDBG=${PHP_FPM_INSTALL_PHPDBG}
          - INSTALL_BLACKFIRE=${INSTALL_BLACKFIRE}
          - INSTALL_SSH2=${PHP_FPM_INSTALL_SSH2}
          - INSTALL_SOAP=${PHP_FPM_INSTALL_SOAP}
          - INSTALL_IMAP=${PHP_FPM_INSTALL_IMAP}
          - INSTALL_AMQP=${PHP_FPM_INSTALL_AMQP}
          - INSTALL_ZIP_ARCHIVE=${PHP_FPM_INSTALL_ZIP_ARCHIVE}
          - INSTALL_BCMATH=${PHP_FPM_INSTALL_BCMATH}
          - INSTALL_GMP=${PHP_FPM_INSTALL_GMP}
          - INSTALL_PHPREDIS=${PHP_FPM_INSTALL_PHPREDIS}
          - INSTALL_MEMCACHED=${PHP_FPM_INSTALL_MEMCACHED}
          - INSTALL_OPCACHE=${PHP_FPM_INSTALL_OPCACHE}
          - INSTALL_EXIF=${PHP_FPM_INSTALL_EXIF}
          - INSTALL_MYSQLI=${PHP_FPM_INSTALL_MYSQLI}
          - INSTALL_INTL=${PHP_FPM_INSTALL_INTL}
          - INSTALL_GHOSTSCRIPT=${PHP_FPM_INSTALL_GHOSTSCRIPT}
          - INSTALL_LDAP=${PHP_FPM_INSTALL_LDAP}
          - INSTALL_PHALCON=${PHP_FPM_INSTALL_PHALCON}
          - INSTALL_SWOOLE=${PHP_FPM_INSTALL_SWOOLE}
          - INSTALL_IMAGE_OPTIMIZERS=${PHP_FPM_INSTALL_IMAGE_OPTIMIZERS}
          - INSTALL_IMAGEMAGICK=${PHP_FPM_INSTALL_IMAGEMAGICK}
          - INSTALL_CALENDAR=${PHP_FPM_INSTALL_CALENDAR}
          - INSTALL_FAKETIME=${PHP_FPM_INSTALL_FAKETIME}
          - INSTALL_IONCUBE=${PHP_FPM_INSTALL_IONCUBE}
          - INSTALL_YAML=${PHP_FPM_INSTALL_YAML}
      volumes:
        - ./php-fpm/php${PHP_VERSION}.ini:/usr/local/etc/php/php.ini
        - ${APP_CODE_PATH_HOST}:${APP_CODE_PATH_CONTAINER}
      expose:
        - "9000"
      extra_hosts:
        - "dockerhost:${DOCKER_HOST_IP}"
      environment:
        - PHP_IDE_CONFIG=${PHP_IDE_CONFIG}
        - DOCKER_HOST=tcp://docker-in-docker:2375
        - FAKETIME=${PHP_FPM_FAKETIME}
      depends_on:
        - workspace
      networks:
        - backend
      links:
        - docker-in-docker

### PHP Worker ############################################
    php-worker:
      build:
        context: ./php-worker
        args:
          - PHP_VERSION=${PHP_VERSION}
          - INSTALL_PGSQL=${PHP_WORKER_INSTALL_PGSQL}
          - INSTALL_BCMATH=${PHP_WORKER_INSTALL_BCMATH}
          - INSTALL_SOAP=${PHP_WORKER_INSTALL_SOAP}
          - INSTALL_ZIP_ARCHIVE=${PHP_WORKER_INSTALL_ZIP_ARCHIVE}
      volumes:
        - ${APP_CODE_PATH_HOST}:${APP_CODE_PATH_CONTAINER}
        - ./php-worker/supervisord.d:/etc/supervisord.d
      depends_on:
        - workspace
      extra_hosts:
        - "dockerhost:${DOCKER_HOST_IP}"
      networks:
        - backend

### NGINX Server #########################################
    nginx:
      build:
        context: ./nginx
        args:
          - PHP_UPSTREAM_CONTAINER=${NGINX_PHP_UPSTREAM_CONTAINER}
          - PHP_UPSTREAM_PORT=${NGINX_PHP_UPSTREAM_PORT}
          - CHANGE_SOURCE=${CHANGE_SOURCE}
      volumes:
        - ${APP_CODE_PATH_HOST}:${APP_CODE_PATH_CONTAINER}
        - ${NGINX_HOST_LOG_PATH}:/var/log/nginx
        - ${NGINX_SITES_PATH}:/etc/nginx/sites-available
        - ${NGINX_SSL_PATH}:/etc/nginx/ssl
      ports:
        - "${NGINX_HOST_HTTP_PORT}:80"
        - "${NGINX_HOST_HTTPS_PORT}:443"
      depends_on:
        - php-fpm
      networks:
        - frontend
        - backend

### Blackfire ########################################
    blackfire:
      image: blackfire/blackfire
      environment:
        - BLACKFIRE_SERVER_ID=${BLACKFIRE_SERVER_ID}
        - BLACKFIRE_SERVER_TOKEN=${BLACKFIRE_SERVER_TOKEN}
      depends_on:
        - php-fpm
      networks:
        - backend

### Apache Server ########################################
    apache2:
      build:
        context: ./apache2
        args:
          - PHP_UPSTREAM_CONTAINER=${APACHE_PHP_UPSTREAM_CONTAINER}
          - PHP_UPSTREAM_PORT=${APACHE_PHP_UPSTREAM_PORT}
          - PHP_UPSTREAM_TIMEOUT=${APACHE_PHP_UPSTREAM_TIMEOUT}
          - DOCUMENT_ROOT=${APACHE_DOCUMENT_ROOT}
      volumes:
        - ${APP_CODE_PATH_HOST}:${APP_CODE_PATH_CONTAINER}
        - ${APACHE_HOST_LOG_PATH}:/var/log/apache2
        - ${APACHE_SITES_PATH}:/etc/apache2/sites-available
      ports:
        - "${APACHE_HOST_HTTP_PORT}:80"
        - "${APACHE_HOST_HTTPS_PORT}:443"
      depends_on:
        - php-fpm
      networks:
        - frontend
        - backend

### MySQL ################################################
    mysql:
      build:
        context: ./mysql
        args:
          - MYSQL_VERSION=${MYSQL_VERSION}
      environment:
        - MYSQL_DATABASE=${MYSQL_DATABASE}
        - MYSQL_USER=${MYSQL_USER}
        - MYSQL_PASSWORD=${MYSQL_PASSWORD}
        - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
        - TZ=${WORKSPACE_TIMEZONE}
      volumes:
        - ${DATA_PATH_HOST}/mysql:/var/lib/mysql
        - ${MYSQL_ENTRYPOINT_INITDB}:/docker-entrypoint-initdb.d
      ports:
        - "${MYSQL_PORT}:3306"
      networks:
        - backend

### MariaDB ##############################################
    mariadb:
      build: ./mariadb
      volumes:
        - ${DATA_PATH_HOST}/mariadb:/var/lib/mysql
        - ${MARIADB_ENTRYPOINT_INITDB}:/docker-entrypoint-initdb.d
      ports:
        - "${MARIADB_PORT}:3306"
      environment:
        - MYSQL_DATABASE=${MARIADB_DATABASE}
        - MYSQL_USER=${MARIADB_USER}
        - MYSQL_PASSWORD=${MARIADB_PASSWORD}
        - MYSQL_ROOT_PASSWORD=${MARIADB_ROOT_PASSWORD}
      networks:
        - backend

### Redis ################################################
    redis:
      build: ./redis
      volumes:
        - ${DATA_PATH_HOST}/redis:/data
      ports:
        - "${REDIS_PORT}:6379"
      networks:
        - backend

### Memcached ############################################
    memcached:
      build: ./memcached
      volumes:
        - ${DATA_PATH_HOST}/memcached:/var/lib/memcached
      ports:
        - "${MEMCACHED_HOST_PORT}:11211"
      depends_on:
        - php-fpm
      networks:
        - backend

### ElasticSearch ########################################
    elasticsearch:
      build: ./elasticsearch
      volumes:
        - elasticsearch:/usr/share/elasticsearch/data
      environment:
        - cluster.name=magentodocker-cluster
        - bootstrap.memory_lock=true
        - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
      ulimits:
        memlock:
          soft: -1
          hard: -1
      ports:
        - "${ELASTICSEARCH_HOST_HTTP_PORT}:9200"
        - "${ELASTICSEARCH_HOST_TRANSPORT_PORT}:9300"
      depends_on:
        - php-fpm
      networks:
        - frontend
        - backend

### Kibana ##############################################
    kibana:
      build: ./kibana
      ports:
        - "${KIBANA_HTTP_PORT}:5601"
      depends_on:
        - elasticsearch
      networks:
        - frontend
        - backend

### MailDev ##############################################
    maildev:
      build: ./maildev
      ports:
        - "${MAILDEV_HTTP_PORT}:80"
        - "${MAILDEV_SMTP_PORT}:25"
      networks:
        - frontend
        - backend

### Varnish ##########################################
    proxy:
      build: ./varnish
      expose:
        - ${VARNISH_PORT}
      environment:
        - VARNISH_CONFIG=${VARNISH_CONFIG}
        - CACHE_SIZE=${VARNISH_PROXY1_CACHE_SIZE}
        - VARNISHD_PARAMS=${VARNISHD_PARAMS}
        - VARNISH_PORT=${VARNISH_PORT}
        - BACKEND_HOST=${VARNISH_PROXY1_BACKEND_HOST}
        - BACKEND_PORT=${VARNISH_BACKEND_PORT}
        - VARNISH_SERVER=${VARNISH_PROXY1_SERVER}
      links:
        - workspace
      networks:
        - frontend

    proxy2:
      build: ./varnish
      expose:
        - ${VARNISH_PORT}
      environment:
        - VARNISH_CONFIG=${VARNISH_CONFIG}
        - CACHE_SIZE=${VARNISH_PROXY2_CACHE_SIZE}
        - VARNISHD_PARAMS=${VARNISHD_PARAMS}
        - VARNISH_PORT=${VARNISH_PORT}
        - BACKEND_HOST=${VARNISH_PROXY2_BACKEND_HOST}
        - BACKEND_PORT=${VARNISH_BACKEND_PORT}
        - VARNISH_SERVER=${VARNISH_PROXY2_SERVER}
      links:
        - workspace
      networks:
        - frontend

### Docker-in-Docker ################################################
    docker-in-docker:
      image: docker:dind
      privileged: true
      volumes:
        - ${APP_CODE_PATH_HOST}:${APP_CODE_PATH_CONTAINER}
      expose:
        - 2375
      networks:
        - backend

### DOCKER-REGISTRY ################################################
    docker-registry:
      build:
        context: ./docker-registry
      volumes:
        - /etc/localtime:/etc/localtime:ro
        - ${DATA_PATH_HOST}/docker-registry:/var/lib/registry
      ports:
        - "${DOCKER_REGISTRY_PORT}:5000"
      networks:
        - backend

### MAILU ################################################
    mailu:
      image: mailu/admin:${MAILU_VERSION}
      volumes:
        - "${DATA_PATH_HOST}/mailu/data:/data"
        - "${DATA_PATH_HOST}/mailu/dkim:/dkim"
        - "${DATA_PATH_HOST}/mailu/webmail:/webmail"
        - /var/run/docker.sock:/var/run/docker.sock:ro
      depends_on:
        - mailu-front
        - mailu-imap
        - mailu-smtp
        - mailu-antispam
        - mailu-antivirus
        - mailu-webdav
        - mailu-admin
        - mailu-webmail
        - mailu-fetchmail
      command: ["sh", "-c", "echo ${MAILU_INIT_ADMIN_USERNAME}@${MAILU_DOMAIN} ${MAILU_INIT_ADMIN_PASSWORD} ;python manage.py advertise ; python manage.py db upgrade ; python manage.py admin ${MAILU_INIT_ADMIN_USERNAME} ${MAILU_DOMAIN} ${MAILU_INIT_ADMIN_PASSWORD} || true;sed -i -- \"s/= Off/= On/g\" /webmail/_data_/_default_/configs/config.ini || true;if grep -Fq \"registration_link_url\" /webmail/_data_/_default_/configs/config.ini;then echo Already set!;else echo \"\" >> /webmail/_data_/_default_/configs/config.ini; echo \"[login]\" >> /webmail/_data_/_default_/configs/config.ini;echo \"registration_link_url = '${MAILU_WEBSITE}${MAILU_WEB_ADMIN}/ui/user/signup'\" >> /webmail/_data_/_default_/configs/config.ini;fi"]
      networks:
        - backend
    mailu-front:
      image: mailu/nginx:${MAILU_VERSION}
      environment:
        - ADMIN=${MAILU_ADMIN}
        - WEB_ADMIN=${MAILU_WEB_ADMIN}
        - WEB_WEBMAIL=${MAILU_WEB_WEBMAIL}
        - WEBDAV=${MAILU_WEBDAV}
        - HOSTNAMES=${MAILU_HOSTNAMES}
        - TLS_FLAVOR=${MAILU_TLS_FLAVOR}
        - MESSAGE_SIZE_LIMIT=${MAILU_MESSAGE_SIZE_LIMIT}
      ports:
        - "${MAILU_HTTP_PORT}:80"
        - "${MAILU_HTTPS_PORT}:443"
        - "110:110"
        - "143:143"
        - "993:993"
        - "995:995"
        - "25:25"
        - "465:465"
        - "587:587"
      volumes:
        - "${DATA_PATH_HOST}/mailu/certs:/certs"
      networks:
        backend:
          aliases:
            - front
    mailu-imap:
      image: mailu/dovecot:${MAILU_VERSION}
      environment:
        - DOMAIN=${MAILU_DOMAIN}
        - HOSTNAMES=${MAILU_HOSTNAMES}
        - POSTMASTER=${MAILU_POSTMASTER}
        - WEBMAIL=${MAILU_WEBMAIL}
        - RECIPIENT_DELIMITER=${MAILU_RECIPIENT_DELIMITER}
      volumes:
        - "${DATA_PATH_HOST}/mailu/data:/data"
        - "${DATA_PATH_HOST}/mailu/mail:/mail"
        - "${DATA_PATH_HOST}/mailu/overrides:/overrides"
      depends_on:
        - mailu-front
      networks:
        backend:
          aliases:
            - imap
    mailu-smtp:
      image: mailu/postfix:${MAILU_VERSION}
      environment:
        - DOMAIN=${MAILU_DOMAIN}
        - HOSTNAMES=${MAILU_HOSTNAMES}
        - RELAYHOST=${MAILU_RELAYHOST}
        - RELAYNETS=${MAILU_RELAYNETS}
        - RECIPIENT_DELIMITER=${MAILU_RECIPIENT_DELIMITER}
        - MESSAGE_SIZE_LIMIT=${MAILU_MESSAGE_SIZE_LIMIT}
      volumes:
        - "${DATA_PATH_HOST}/mailu/data:/data"
        - "${DATA_PATH_HOST}/mailu/overrides:/overrides"
      depends_on:
        - mailu-front
      networks:
        backend:
          aliases:
            - smtp
    mailu-antispam:
      image: mailu/rspamd:${MAILU_VERSION}
      volumes:
        - "${DATA_PATH_HOST}/mailu/filter:/var/lib/rspamd"
        - "${DATA_PATH_HOST}/mailu/dkim:/dkim"
        - "${DATA_PATH_HOST}/mailu/overrides/rspamd:/etc/rspamd/override.d"
      depends_on:
        - mailu-front
      networks:
        backend:
          aliases:
            - antispam
    mailu-antivirus:
      image: mailu/clamav:${MAILU_VERSION}
      volumes:
        - "${DATA_PATH_HOST}/mailu/filter:/data"
      networks:
        backend:
          aliases:
            - antivirus
    mailu-webdav:
      image: mailu/${MAILU_WEBDAV}:${MAILU_VERSION}
      volumes:
        - "${DATA_PATH_HOST}/mailu/dav:/data"
      networks:
        backend:
          aliases:
            - webdav
    mailu-admin:
      image: mailu/admin:${MAILU_VERSION}
      environment:
        - DOMAIN=${MAILU_DOMAIN}
        - HOSTNAMES=${MAILU_HOSTNAMES}
        - POSTMASTER=${MAILU_POSTMASTER}
        - SECRET_KEY=${MAILU_SECRET_KEY}
        - AUTH_RATELIMIT=${MAILU_AUTH_RATELIMIT}
        - TLS_FLAVOR=${MAILU_TLS_FLAVOR}
        - DISABLE_STATISTICS=${MAILU_DISABLE_STATISTICS}
        - DMARC_RUA=${MAILU_DMARC_RUA}
        - DMARC_RUF=${MAILU_DMARC_RUF}
        - WELCOME=${MAILU_WELCOME}
        - WELCOME_SUBJECT=${MAILU_WELCOME_SUBJECT}
        - WELCOME_BODY=${MAILU_WELCOME_BODY}
        - WEB_ADMIN=${MAILU_WEB_ADMIN}
        - WEB_WEBMAIL=${MAILU_WEB_WEBMAIL}
        - WEBSITE=${MAILU_WEBSITE}
        - WEBMAIL=${MAILU_WEBMAIL}
        - SITENAME=${MAILU_SITENAME}
        - PASSWORD_SCHEME=${MAILU_PASSWORD_SCHEME}
        - RECAPTCHA_PUBLIC_KEY=${MAILU_RECAPTCHA_PUBLIC_KEY}
        - RECAPTCHA_PRIVATE_KEY=${MAILU_RECAPTCHA_PRIVATE_KEY}
      volumes:
        - "${DATA_PATH_HOST}/mailu/data:/data"
        - "${DATA_PATH_HOST}/mailu/dkim:/dkim"
        - /var/run/docker.sock:/var/run/docker.sock:ro
      depends_on:
        - redis
      networks:
        backend:
          aliases:
            - admin
    mailu-webmail:
      image: "mailu/${MAILU_WEBMAIL}:${MAILU_VERSION}"
      volumes:
        - "${DATA_PATH_HOST}/mailu/webmail:/data"
      networks:
        backend:
          aliases:
            - webmail
    mailu-fetchmail:
      image: mailu/fetchmail:${MAILU_VERSION}
      environment:
        - FETCHMAIL_DELAY=${MAILU_FETCHMAIL_DELAY}
      volumes:
        - "${DATA_PATH_HOST}/mailu/data:/data"
      networks:
        backend:
          aliases:
            - fetchmail
