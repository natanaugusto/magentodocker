# The Magentodocker setup guide

##### This guide will show you how setup a magento project with a docker solution(Copied from laradock) 
---
##### First steps
- Install the docker and docker-compose
- Clone the magentodocker from bitbucket [repo](https://bitbucket.org/natanaugusto/magentodocker)
- Copy de env-example file
- Up some containers

Install the docker
- Debian/Ubuntu [link](http://lmgtfy.com/?q=Install+last+version+docker+docker-compose+debian+ubunutu)
- Arch/Manjaro [link](http://lmgtfy.com/?q=Install+last+version+docker+docker-compose+manjaro+arch)
- RedHat/Fedora [link](http://lmgtfy.com/?q=Install+last+version+docker+docker-compose+red+hat+fedora)

Clone the magentodocker repo
```bash
git clone https://bitbucket.org/natanaugusto/magentodocker
cd magentodocker
cp env-example .env
```
Now we goes up some basic containers
```bash
# This command will take a couple of hours to conclue
docker-compose up -d nginx mariadb
```
I recommend the MariaDB instead MySQL. The MySQL sometimes crash and no persist data correctly

#### Running a magento2 on magentodocker
- Create a magento2 project with composer
- Configure the nginx to run the magento project
- Create the database
- Install the magento2 project

Create the magento2 project
```bash
# If you want, you can use the workspace's composer
# To enter on workspace
docker-compose run workspace bash

# Now inside the workspace as the root user
composer create-project --repository=https://repo.magento.com/ magento/project-community-edition example

# Exit from the docker workspace
exit
```

Now we going config the nginx
```bash
# Copy the nginx config example inside the nginx/sites
cp nginx/sites/magento.conf.example nginx/sites/example.conf
vim nginx/sites/example.conf
```
file: nginx/sites/example.conf
```nginx
  server {
      listen 80;
      listen 443 ssl http2;
 
      server_name example.local;
      ssl_certificate     /etc/nginx/ssl/default.crt
      ssl_certificate_key /etc/nginx/ssl/default.key;
      set $MAGE_ROOT /var/www/example;
      include /var/www/example/nginx.conf.sample;
 
      error_log /var/log/nginx/example_error.log;
      access_log /var/log/nginx/example_access.log;
  }
```
Now reset the nginx
```bash
docker-compose reset nginx
```

Now we going create the database
```bash
# If you want, you can use the mariadb container to create
docker-compose exec mariadb bash
# Now you is inside the mariadb docker container
mysql -u root -proot

# Or for a external client
mysql -u root -proot -h 127.0.0.1
```

Create the database
```sql
CREATE DATABASE `example` CHARACTER SET 'utf8'
```
Now lets go install the magento
```bash
# Enter on your workspace
docker-compose run workspace bash

# In your workspace goes to the root of your project
cd example

# Now run the bin/magento install
php bin/magento setup:install --base-url=http://example.local --db-host=mariadb --db-name=Example --db-user=root --db-password=root --admin-firstname=Magento --admin-lastname=User --admin-email=my@email.com --admin-user=admin --admin-password=password123 --language=pt_BR --currency=BRL --timezone=America/Sao_Paulo --use-rewrites=1 --backend-frontname=admin
```

#### Now we goes up the elasticsearch
```bash
# The elasticsearch needs a lot of memory
# So, we need to setup that
sudo sysctl vm.max_map_count=262144

# Now web up the container
docker-compose up -d elasticsearch
```

Thanks
